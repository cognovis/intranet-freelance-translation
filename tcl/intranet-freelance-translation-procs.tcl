# /packages/intranet-freelance-translation/tcl/intranet-freelance-translation-procs.tcl
#
# Copyright (c) 2003-2006 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

ad_library {
    Common procedures to implement translation freelancer functions:
    @author frank.bergmann@project-open.com
}


# ---------------------------------------------------------------
# Freelance Translation Member Select Component
# ---------------------------------------------------------------

ad_proc im_freelance_trans_member_select_component_join_prices {
    hash
    params
} {
    Simplify the display of prices that are very similar to each other.
    This function checks if the two lists of price parameters only differ
    in at most 1 list element. In this case, it will join the two parameter
    lists by concatenating the one list element that is different.
    Otherwise it will just return two list elements.
} {
    # Check if this is the first price.
    if {"" == $hash} { return [list $params] }

    # Ok, there is at least one parameter list in the list "hash".
    # Split the list into its last element, and the first elements
    set hash_last [lindex $hash end]
    set hash_first [lrange $hash 0 end-1]

    # Compare the hash_last (simple!) list with the params simple list
    set len [llength $params]
    if {[llength $hash_last] > $len} { set len [llength $hash_last] }
    set diffs 0
    set param_idx -1
    for {set i 0} {$i < $len} {incr i} {
	if {[lindex $params $i] != [lindex $hash_last $i]} {
	    incr diffs
	    set param_idx $i
	}
    }

    if {1 == $diffs} {
	# Combine the two elements
	set result [list]
	for {set i 0} {$i < $len} {incr i} {
	    if {$i != $param_idx} {
		lappend result [lindex $params $i]
	    } else {
		lappend result "[lindex $hash_last $i] [lindex $params $i]"
	    }
	}
	lappend hash_first $result
	return $hash_first
    } else {
	# Just return the appended lists
	lappend hash_first $hash_last
	lappend hash_first $params
	return $hash_first
    }
}


# ---------------------------------------------------------------
# Changes by Malte Sussdorff malte.sussdorff@cognovis.de
# ---------------------------------------------------------------

# this proc is only used without quality module
ad_proc im_freelance_trans_member_select_component {
    object_id
    return_url

} {
    Component that returns a formatted HTML table that allows
    to select freelancers according to the characteristics of
    the current project.
} {
    if {"" == $return_url} { set return_url [im_url_with_query] }
    set params [list [list base_url "/intranet-freelance-tranlsation/"] [list object_id $object_id] [list return_url $return_url]]
    set result [ad_parse_template -params $params "/packages/intranet-freelance-translation/lib/freelance-trans-member"]
    return [string trim $result]
}

ad_proc -public im_freelance_trans_worked_with_customer {
	-freelancer_id
	-customer_id
} {
	Returns a list of times worked with the customer and in general
} {
	return [util_memoize [list im_freelance_trans_worked_with_customer_helper -freelancer_id $freelancer_id -customer_id $customer_id] 600]
}

ad_proc -public im_freelance_trans_worked_with_customer_helper {
	-freelancer_id
	-customer_id
} {
	Returns a list of times worked with the customer and in general
} {
	set task_types [db_list task_types "select category from im_categories where category_type = 'Intranet Trans Task Type'"]
	
	set return_key_value_list [list]
	foreach task_type $task_types {
		if {[im_column_exists "im_trans_tasks" "${task_type}_id"]} {
			set no_times_for_customer [db_string $task_type "select count(*) from
				(select distinct tree_root_key(p.tree_sortkey)
				 	from im_trans_tasks t, im_projects p
				 	where (p.company_id = :customer_id or p.final_company_id = :customer_id)
				 	and ${task_type}_id = :freelancer_id and p.project_id = t.project_id) t"]
			set no_times [db_string $task_type "select count(distinct t.project_id) from im_trans_tasks t where ${task_type}_id = :freelancer_id"]
			lappend return_key_value_list [list ${task_type}_customer $no_times_for_customer]
			lappend return_key_value_list [list ${task_type} $no_times]
		}
	}
	
	return $return_key_value_list
}
