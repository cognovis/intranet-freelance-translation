    # ------------------------------------------------
    # Security
  
    set user_id [ad_get_user_id]

    #
    if {![db_string skills_p "select 1 from im_object_freelance_skill_map where object_id = :object_id limit 1" -default 0]} {
        set filter_html ""
        set select_freelance ""
    }

    set left_navbar "Malte Rules"
    # ------------------------------------------------
    # Parameter Logic
    #
    # Get the freel_trans_order_by variable from the http header
    # because we can't trust that the embedding page will pass
    # this param into this component.

    set current_url [ad_conn url]
    set header_vars [ns_conn form]
    set var_list [ad_ns_set_keys $header_vars]

    # set local TCL vars from header vars
    ad_ns_set_to_tcl_vars $header_vars

    # Remove the "freel_trans_order_by" from the var_list
    set order_by_pos [lsearch $var_list "freel_trans_order_by"]
    if {$order_by_pos > -1} {
	    set var_list [lreplace $var_list $order_by_pos $order_by_pos]
    }

    if {![info exists freel_trans_order_by]} {
        set freel_trans_order_by [parameter::get_from_package_key -package_key intranet-freelance-translation -parameter FreelanceListSortOder -default "S-Word"]
    }
    set freel_trans_order_by [string tolower $freel_trans_order_by]

#    ad_return_complaint 1 $freel_trans_order_by

    # Sort order
    switch $freel_trans_order_by {
        s-word {
	        # Order by the number of time the freelance has worked before with the customer
            set freel_trans_order_by_sql "f.no_times_worked_for_customer DESC"
            set order_freelancer_sql "no_times_worked_for_customer DESC"
        }
        worked_before {
            # Order by the number of time the freelance has worked before with the customer
            set freel_trans_order_by_sql "f.no_times_worked_for_customer DESC"
            set order_freelancer_sql "no_times_worked_for_customer DESC"
        }
        name {
            set freel_trans_order_by_sql "f.name"
            set order_freelancer_sql "user_name"
        }
        default {
            set freel_trans_order_by_sql "min(p.price), p.source_language_id, p.target_language_id, p.task_type_id, p.subject_area_id, p.file_type_id"
            set order_freelancer_sql "user_name"
        }
    }

    # ------------------------------------------------
    # Constants

    # Default Role: "Full Member"
    set default_role_id 1300
    
    # How many static columns?
    set colspan 3

    set bgcolor(0) " class=roweven "
    set bgcolor(1) " class=rowodd "

    set source_lang_skill_type 2000
    set target_lang_skill_type 2002

    set skill_filter_ids [list]
    
    # Project's Source & Target Languages
    db_0or1row source_langs "select  source_language_id, ch.parent_id
                from    im_projects left outer join im_category_hierarchy ch on (child_id = source_language_id)
                where   project_id = :object_id"
    
    set source_languages [list]
    if {[exists_and_not_null parent_id]} {
        set source_language_ids [im_sub_categories $parent_id]
    } else {
	if {[exists_and_not_null source_language_id]} {
	    set source_language_ids [im_sub_categories $source_language_id]
	} else {
	    set source_language_ids ""
	}
    }
    
    foreach source_lang_id $source_language_ids {
        lappend source_languages [list [im_category_from_id $source_lang_id] $source_lang_id]
    }

    if {[info exists skill_2000]} {
        set source_language_ids [im_sub_categories $skill_2000]
    } else {
	    set skill_2000 $source_language_id
	    set source_language_ids [im_sub_categories $skill_2000]
    }
        
    set target_languages [list]
    set target_language_ids [list]
    db_foreach target_langs "
		select  language_id as target_language_id, parent_id
		from	im_target_languages left outer join im_category_hierarchy ch on (child_id = language_id)
		where	project_id = :object_id
    " {
        if {[exists_and_not_null parent_id]} {
            set target_ids [im_sub_categories $parent_id]
        } else {
            set target_ids [im_sub_categories $target_language_id]
        }
        foreach target_id $target_ids {
            if {[lsearch $target_language_ids $target_id]<0} {
                lappend target_language_ids $target_id
            }
        }
    }
    
    if {$target_language_ids ne ""} {
	    # Sort them by name
	    db_foreach target_lang_id "
	    	select im_name_from_id(category_id) as target_lang, category_id
	    	from im_categories
	    	where category_id in ([template::util::tcl_to_sql_list $target_language_ids])
	    	order by target_lang
	    " {
	        lappend target_languages [list $target_lang $category_id]
	    }
	}
	
    if {![exists_and_not_null skill_2002]} {
		set skill_2002 [db_string target_lang "select language_id from im_target_languages where project_id = :object_id limit 1"]
	}

	set target_language_ids [im_sub_categories $skill_2002]

	# Set the pass through values
	set q_source_language_id $skill_2000
	set q_target_language_id $skill_2002

    # ---------------------------------------------------------------
    # Filter with Dynamic Fields
    # ---------------------------------------------------------------

    set form_id "freelancer_filter"
    set form_mode "edit"
    
    set project_url [export_vars -base "/intranet/projects/view" -url {{project_id $object_id}}]
    set return_url [export_vars -base "/intranet/member-add" -url {object_id {return_url $project_url}}]
    
    ad_form \
        -name $form_id \
        -action $current_url \
        -mode $form_mode \
        -method GET \
        -cancel_url [export_vars -base "/intranet/member-add" -url {object_id return_url}] \
        -export {return_url object_id} \
        -form {
            {skill_2000:text(select),optional {label "[im_category_from_id 2000]"} {options $source_languages} {value $source_language_id}}
            {skill_2002:text(select),optional {label "[im_category_from_id 2002]"} {options $target_languages} {value $skill_2002}}
        }
        
    # Append the skills to the filter
    
    set skill_type_list [list 2000 2002]
    set skill_type_sql "\t\tim_freelance_skill_list_html(m.member_id, 2000) as skill_2000,\n"
    append skill_type_sql "\t\tim_freelance_skill_list_html(m.member_id, 2002) as skill_2002,\n"

    db_foreach skill_types {select skill_type_id, object_type_id, display_mode, aux_string1, aux_int1
        from im_freelance_skill_type_map, im_categories, im_projects
        where object_type_id = project_type_id and project_id = :object_id and category_id = skill_type_id and display_mode != 'none' and skill_type_id not in (2000,2002) order by im_categories.sort_order} {
        
        set attribute_name "skill_$skill_type_id"
        ad_form -extend -name $form_id -form {
            {${attribute_name}:text(im_category_tree),optional {label "[im_category_from_id $skill_type_id]"} {custom {category_type "$aux_string1" translate_p 1}} }
        }
        
        if {[exists_and_not_null $attribute_name]} {
            lappend required_skill_ids [set $attribute_name]
        }
        
        lappend skill_type_list $skill_type_id
        if {[exists_and_not_null $attribute_name]} {
	        append skill_type_sql "\t\tim_freelance_skill_list_html(m.member_id, $skill_type_id, [set $attribute_name]) as skill_$skill_type_id,\n"
	    } else {
		    append skill_type_sql "\t\tim_freelance_skill_list_html(m.member_id, $skill_type_id) as skill_$skill_type_id,\n"
	    }
        if {[exists_and_not_null skill_$skill_type_id]} {
	        foreach skill_id [im_sub_categories [set skill_$skill_type_id]] {
       			lappend skill_filter_ids $skill_id
       		}
        }
    }
    
    # Overwrite with the project skills unless otherwise specified
    db_foreach required_project_skill {
        select skill_type_id, skill_id, aux_string2 from im_object_freelance_skill_map, im_categories where object_id = :object_id and skill_type_id not in (2000,2002) and skill_type_id = category_id
    } {
        if {![info exists skill_$skill_type_id]} {
	    if {[template::element::exists $form_id "skill_$skill_type_id"]} {
		template::element::set_value $form_id "skill_$skill_type_id" $skill_id
	    }
	    foreach skill_id [im_sub_categories $skill_id] {
		lappend skill_filter_ids $skill_id
	    }
        }
        if {$aux_string2 ne ""} {
	        set $aux_string2 $skill_id
        }
    }
    
    # Compile and execute the formtemplate if advanced filtering is enabled.
    eval [template::adp_compile -string {<formtemplate id="$form_id" style="tiny-plain-po"></formtemplate>}]
    set filter_html "[im_box_header "Freelancer Filtern"] $__adp_output [im_box_footer]"
    
    # ------------------------------------------------
    # Put together the main SQL
	set customer_id [db_string company "select coalesce(final_company_id,company_id) as company_id from im_projects where project_id = :object_id" -default "0"]

    set company_id $customer_id

    set skill_extra_where ""
    if {$skill_filter_ids ne ""} {
	foreach skill_filter_id $skill_filter_ids {
	    append skill_extra_where " AND $skill_filter_id in (select skill_id from im_freelance_skills ifs where skill_type_id not in (2000,2002) and ifs.user_id = m.member_id)"
	}
        set skills_extra_from ",(select category_id from im_categories where category_id in ([template::util::tcl_to_sql_list $skill_filter_ids])) sf
"
	if {[exists_and_not_null subject_area_id]} {
	    set price_extra_where "and (p.subject_area_id = :subject_area_id or
			p.subject_area_id is null)"
	} else {
	    set price_extra_where ""
	}
    } else {
        set skills_extra_from ",im_categories sf"
        set price_extra_where ""
    }

    
    if {$source_language_ids ne ""} {
        set source_lang_from ",(select user_id from im_freelance_skills where skill_type_id = 2000 and skill_id in ([template::util::tcl_to_sql_list $source_language_ids])) source_lang"
        set source_lang_where "AND m.member_id = source_lang.user_id"
    } else {
        set source_lang_from ""
        set source_lang_where ""
        set source_lang ""
    }
    
    if {$target_language_ids ne ""} {
        set target_lang_from ",(select user_id from im_freelance_skills where skill_type_id = 2002 and skill_id in ([template::util::tcl_to_sql_list $target_language_ids])) target_lang"
        set target_lang_where " AND m.member_id = target_lang.user_id"
    } else {
        set target_lang_from ""
        set target_lang_where ""
        set target_lang ""
    }
    
    set company_status_ids [im_sub_categories [im_company_status_active_or_potential]]
    
    set freelance_sql "
	select distinct
		im_name_from_user_id(m.member_id) as user_name,
		im_name_from_user_id(m.member_id) as name,
		$skill_type_sql
		m.member_id as user_id,
        m.member_id as user_id_from_search,
		im_user_worked_for_company ( m.member_id, $company_id ) as no_times_worked_for_customer,
        (
			select	count(*) as cnt
			from	(select	object_id_two as user_id,
					p.project_id
				from	acs_rels r,
					im_projects p
				where	p.company_id = :customer_id
					and r.object_id_one = p.project_id
                    and object_id_two = m.member_id
				) ww
        ) as worked_with_cust_cnt
	from
		group_member_map m,
		membership_rels mr,
		(select object_id_two from acs_rels r, im_companies c where c.company_id = r.object_id_one and company_status_id in ([template::util::tcl_to_sql_list $company_status_ids])) f
        $source_lang_from
        $target_lang_from
    where
		m.group_id = acs__magic_object_id('registered_users'::character varying) AND
		m.rel_id = mr.rel_id AND
		m.container_id = m.group_id AND
		m.rel_type::text = 'membership_rel'::text AND
		mr.member_state::text = 'approved'::text AND
		f.object_id_two = m.member_id
		$skill_extra_where
        $source_lang_where
        $target_lang_where
	order by
		$order_freelancer_sql
    "

    # ------------------------------------------------
    # Mix Freelance Info with Prices

    set table_rows [list]
    set freelancer_ids [list]
    db_foreach freelance $freelance_sql {
        lappend freelancer_ids $user_id
	        
	    # Set the default sort value to a random number, otherwise qsort will fail (below)
        set sort_val [expr 999999999 + 999999999 * rand()]
        if {[info exists sort_hash($user_id)]} { set sort_val $sort_hash($user_id) }
	
        set row [list $user_id $name $sort_val]
	
        foreach skill_type_id $skill_type_list {
            lappend row [expr "\$skill_$skill_type_id"]
        }
        lappend row $no_times_worked_for_customer
        lappend table_rows $row
    }

    set sorted_table_rows $table_rows
    if { ![info exists freel_trans_order_by] } {
        set sorted_table_rows [qsort $table_rows [lambda {s} { lindex $s 2 }]]
    }
    # ------------------------------------------------
    # Determine the price ranges per freelancer
    set price_sql "
	select
		f.user_id,
		c.company_id,
		p.uom_id,
		im_category_from_id(p.task_type_id) as task_type,
		im_category_from_id(p.source_language_id) as source_language,
		im_category_from_id(p.target_language_id) as target_language,
		im_category_from_id(p.subject_area_id) as subject_area,
		im_category_from_id(p.file_type_id) as file_type,
		min(p.price) as min_price,
		max(p.price) as max_price,
		f.no_times_worked_for_customer as no_times_worked_for_customer
	from
		($freelance_sql) f
		LEFT OUTER JOIN acs_rels uc_rel ON (f.user_id = uc_rel.object_id_two)
		LEFT OUTER JOIN im_trans_prices p ON (uc_rel.object_id_one = p.company_id),
		im_companies c
	where
		p.company_id = c.company_id
		and (p.source_language_id in ([template::util::tcl_to_sql_list $source_language_ids]) or
			p.source_language_id is null)
		and (p.target_language_id in ([template::util::tcl_to_sql_list $target_language_ids]) or
			p.target_language_id is null)
		$price_extra_where
	group by
		f.user_id,
		c.company_id,
		p.uom_id,
		p.task_type_id,
		p.source_language_id,
		p.target_language_id,
		p.subject_area_id,
		p.file_type_id,
		f.name,
		f.no_times_worked_for_customer
	order by task_type,
		$freel_trans_order_by_sql
    "

    db_foreach price_hash $price_sql {
        set key "$user_id-$uom_id"

        # Calculate the base cell value
        set price_append "$min_price - $max_price"
        if {$min_price == $max_price} { set price_append "$min_price" }
        # Add the list of parameters
        set param_list [list $price_append $source_language $target_language]
        if {"" == $source_language && "" == $target_language} { set param_list [list $price_append] }
        if {"" != $subject_area} { lappend param_list $subject_area }
        if {"" != $task_type} { lappend param_list $task_type }
        if {"" != $file_type} { lappend param_list $file_type }

        # Update the hash table cell
        set hash ""
        if {[info exists price_hash($key)]} { set hash $price_hash($key) }
        set hash [im_freelance_trans_member_select_component_join_prices $hash $param_list]
        set price_hash($key) $hash

        # deal with sorting the array be one of the
        switch $freel_trans_order_by {
            "s-word" {
                if {$uom_id == 324} {
                    set sort_hash($user_id) [expr ($min_price + $max_price) / 2.0]
                }
            }
            "hour" {
                if {$uom_id == 320} {
                    set sort_hash($user_id) [expr ($min_price + $max_price) / 2.0]
		        }
            }
            default { }
        }
    }

    foreach key [array names price_hash] {
        set values $price_hash($key)
        set result ""
        foreach value $values {
            append result "<nobr>[lindex $value 0] {[lrange $value 1 end]}</nobr><br>\n"
        }
        set price_hash($key) $result
    }


    set uom_listlist [db_list_of_lists uom_list "
        select uom_id, im_category_from_id(uom_id)
        from (select distinct uom_id from ($price_sql) t) t
        order by uom_id
    "]


	set uom_listlist "{324 S-Word} {320 Hour}"
    # Remove "freel_trans_order_by" from the var_list
    set order_by_pos [lsearch $var_list "freel_trans_order_by"]
    if {$order_by_pos > -1} {
        set var_list [lreplace $var_list $order_by_pos $order_by_pos]
    } else {
        set var_list $var_list
    }


# ----------------------------------------------------------------------

set role_id 1300 ;# default role_id for full member

set extend_vars {cnt_pretty}

set elements {
    user_name {
        label {[_ intranet-freelance.Freelance]}
        display_template {
            <a href="/intranet/users/view?user_id=@multirow_freelancers.user_id@">@multirow_freelancers.user_name@</a>
        }
    }
    worked_with_customer_before {
        label {[lang::message::lookup "" intranet-freelance-translation.Worked_with_Customer_Before "Worked With Cust Before?"]}
        display_template { @multirow_freelancers.cnt_pretty@ }
    }
}

# add a column for each skill type
foreach skill_type_id $skill_type_list {
    set skill_type [im_category_from_id $skill_type_id]
    regsub { } $skill_type "_" skill_type_mangled
    lappend elements skill_type_$skill_type_id [list \
        label [lang::message::lookup "" intranet-freelance.$skill_type_mangled $skill_type] \
        display_template "@multirow_freelancers.skill_type_html_${skill_type_id};noquote@"]

    lappend extend_vars "skill_type_html_${skill_type_id}"

}

# Add a column for each UoM where there is a price per user.
foreach uom_tuple $uom_listlist {
    set uom_id [lindex $uom_tuple 0]
    set title [lindex $uom_tuple 1]
    lappend elements uom_$uom_id [list \
        label $title \
        display_template "@multirow_freelancers.uom_html_${uom_id};noquote@"]

    lappend extend_vars "uom_html_${uom_id}"
}

set list_name "list_freelancers"
set multirow_name "multirow_freelancers"
::template::list::create \
	-name $list_name \
	-multirow $multirow_name \
	-elements $elements \
    -key "user_id_from_search" \
    -bulk_action_export_vars {return_url project_id object_id role_id q_source_language_id q_target_language_id} \
    -bulk_actions {
        "Add" "member-add-2?" "Add as full member"
		"Add & Notify" "member-add-2?notify_asignee=1" "Add as full member and notify assignee"
    }

set trans_steps [split [db_string get_steps "select aux_string1 from im_categories c, im_projects p where p.project_id = :object_id and p.project_type_id = c.category_id" -default ""] " "]


set extend_script {
	template::util::list_of_lists_to_array [im_freelance_trans_worked_with_customer -freelancer_id $user_id -customer_id $customer_id] worked_with_arr
	
	set cnt_number_list [list]
	set cnt_cust_number_list [list]
	foreach task_type $trans_steps {
		lappend cnt_number_list "$worked_with_arr($task_type) $task_type"
		lappend cnt_cust_number_list "$worked_with_arr(${task_type}_customer) $task_type"
	}
    switch $worked_with_cust_cnt {
        "" - 0 {
	        set cnt_pretty "[lang::message::lookup "" intranet-freelance-translation.never "never"]  ([join $cnt_number_list " / "])"
	    }
        1 {
	        set cnt_pretty "[lang::message::lookup "" intranet-freelance-translation.never "once"]  ([join $cnt_number_list " / "])"
	    }
        default {
	        set cnt_pretty "[lang::message::lookup "" intranet-freelance-translation.N_times "$worked_with_cust_cnt times"] ([join $cnt_cust_number_list " / "])"
	    }
    }

    # each skill type html is stored in columns with names e.g. skill_2000, and so on
	foreach skill_type_id $skill_type_list {
        set skill_type_html_${skill_type_id} [set skill_${skill_type_id}]
    }

    foreach uom_tuple $uom_listlist {
        set uom_id [lindex $uom_tuple 0]
	    set key "${user_id}-${uom_id}"
	    set val ""
	    if { [info exists price_hash($key)] } {
            set val $price_hash($key)
        }
        set uom_html_${uom_id} ${val}
    }

}

db_multirow -extend $extend_vars $multirow_name select_$multirow_name $freelance_sql $extend_script

